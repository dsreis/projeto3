﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projeto3.Domain;

namespace Projeto3.Repository.Interface
{
    public interface IClienteRepository : IRepository<Cliente>
    {

    }
}
