﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projeto3.Domain;

namespace Projeto3.Repository.Interface
{
    interface IProcessoRepository : IRepository<Processo>
    {
        List<Processo> GetByAtivo(bool ativo);
        List<Processo> GetByValor(float valor);
        float GetValorMedioCliente(int IdCliente);

    }
}
