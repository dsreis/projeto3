﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto3.Domain
{
    public class Processo
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Estado { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataInicio { get; set; }
        public float Valor { get; set; }
        public Cliente Cliente { get; set; }
    }
}
